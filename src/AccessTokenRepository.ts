import moment = require("moment");
import {AccessTokenRefresher} from "./AccessTokenRefresher";

export const TOKEN_LOCAL_STORAGE_KEY = 'scorm_proxy.access_token';
export const TOKEN_EXPIRES_AT_LOCAL_STORAGE_KEY = 'scorm_proxy.access_token_expires_at';

export class AccessTokenRepository {
    private readonly scormProxyUrl: string;
    private readonly scormProxyClientId: string;
    private readonly scormProxyClientSecret: string;

    private tokenRefresher: AccessTokenRefresher;

    constructor(scormProxyUrl: string, scormProxyClientId: string, scormProxyClientSecret: string) {
        this.scormProxyUrl = scormProxyUrl;
        this.scormProxyClientId = scormProxyClientId;
        this.scormProxyClientSecret = scormProxyClientSecret;

        this.tokenRefresher = new AccessTokenRefresher(scormProxyUrl, scormProxyClientId, scormProxyClientSecret);
    }

    public getToken(): string | null {
        if (this.tokenDoesNotExistInLocalStorage() || this.tokenInLocalStorageIsExpired()) {
            this.refreshToken();
        }

        return this.getTokenFromLocalStorage();
    }

    private tokenInLocalStorageIsExpired(): boolean {
        const expiresAtFromLocalStorage = localStorage.getItem(TOKEN_EXPIRES_AT_LOCAL_STORAGE_KEY);
        const expiresAt = expiresAtFromLocalStorage ? moment(expiresAtFromLocalStorage) : null;
        return expiresAt === null || moment() >= expiresAt;
    }

    private tokenDoesNotExistInLocalStorage(): boolean {
        return this.getTokenFromLocalStorage() === null;
    }

    private getTokenFromLocalStorage(): string | null {
        return localStorage.getItem(TOKEN_LOCAL_STORAGE_KEY);
    }

    private refreshToken(): void {
        const result = this.tokenRefresher.refreshToken();

        localStorage.setItem(TOKEN_LOCAL_STORAGE_KEY, result.token);
        localStorage.setItem(TOKEN_EXPIRES_AT_LOCAL_STORAGE_KEY, result.expiresAt);
    }
}
