import {ScormProxy} from "../ScormProxy"
import {ScormCompletionPayload} from "../types/ScormCompletionPayload"
import {EVENT_TYPES} from "../eventTypes"
import * as sinon from 'sinon'
import {AccessTokenRepository} from "../AccessTokenRepository";

const scormProxyUrl = 'http://localhost:8000'
const clientId = 'client_id'
const clientSecret = 'client_secret'

const zipUrl = 'https://scorm-storage.test/scorm.zip';

const scormCompletionPayload: ScormCompletionPayload = {
    passed: false,
    score: 0,
    questionResults: [],
}

afterEach(() => {
    sinon.restore()
})

it('getUrl', () => {
    const token = 'token'
    const accessTokenRepositoryGetTokenStub = sinon.stub(AccessTokenRepository.prototype, 'getToken').returns(token)

    const zipArchiveUrl = 'http://some-scorm-storage.test/storage/scorm.zip'
    const scormProxy = new ScormProxy(scormProxyUrl, clientId, clientSecret)

    const result = scormProxy.getUrl(zipArchiveUrl)

    expect(result).toEqual(`${scormProxyUrl}/open_scorm?url=${zipArchiveUrl}&token=${token}`)
    sinon.assert.calledOnce(accessTokenRepositoryGetTokenStub)
})

describe('listenCourseCompleted', () => {
    it('first call', (done) => {
        const windowRemoveEventListenerSpy = sinon.spy(window, 'removeEventListener');
        const scormProxy = new ScormProxy(scormProxyUrl, clientId, clientSecret);

        scormProxy.listenCourseCompleted(zipUrl, (result: ScormCompletionPayload) => {
            expect(result).toEqual(scormCompletionPayload);
            sinon.assert.notCalled(windowRemoveEventListenerSpy);
            done();
        });

        window.postMessage({
            type: EVENT_TYPES.SCORM_COMPLETED,
            data: scormCompletionPayload,
            zipUrl,
        }, '*');
    });

    it('second call', (done) => {
        const windowRemoveEventListenerSpy = sinon.spy(window, 'removeEventListener');
        const scormProxy = new ScormProxy(scormProxyUrl, clientId, clientSecret);

        scormProxy.listenCourseCompleted(zipUrl, (progress: ScormCompletionPayload) => '');
        scormProxy.listenCourseCompleted(zipUrl, (result: ScormCompletionPayload) => {
            expect(result).toEqual(scormCompletionPayload);
            sinon.assert.calledOnce(windowRemoveEventListenerSpy);
            sinon.assert.calledWith(windowRemoveEventListenerSpy, 'message', sinon.match((value) => {
                return !!value && typeof value === 'function';
            }));
            done();
        });

        window.postMessage({
            type: EVENT_TYPES.SCORM_COMPLETED,
            data: scormCompletionPayload,
            zipUrl,
        }, '*');
    });

    it('received post message with wrong zipUrl', () => {
        const windowRemoveEventListenerSpy = sinon.spy(window, 'removeEventListener');
        const scormProxy = new ScormProxy(scormProxyUrl, clientId, clientSecret);

        scormProxy.listenCourseCompleted(zipUrl, (progress: ScormCompletionPayload) => {
            fail('listenCourseCompleted should not resolve when event with wrong zip url is received');
        });

        sinon.assert.notCalled(windowRemoveEventListenerSpy);

        window.postMessage({
            type: EVENT_TYPES.SCORM_COMPLETED,
            data: {
                passed: false,
                score: 0,
                questionResults: [],
            },
            zipUrl: 'https://wrong-scorm-url.test/scorm.zip',
        }, '*');
    });
});

it('flushListeners', () => {
    const windowRemoveEventListenerSpy = sinon.spy(window, 'removeEventListener');
    const scormProxy = new ScormProxy(scormProxyUrl, clientId, clientSecret);
    scormProxy.listenCourseCompleted(zipUrl, (progress: ScormCompletionPayload) => '');

    scormProxy.flushListeners();

    sinon.assert.calledOnce(windowRemoveEventListenerSpy);
    sinon.assert.calledWith(windowRemoveEventListenerSpy, 'message', sinon.match((value) => {
        return !!value && typeof value === 'function';
    }));
});


