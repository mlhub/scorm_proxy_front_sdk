import {
    AccessTokenRepository,
    TOKEN_EXPIRES_AT_LOCAL_STORAGE_KEY,
    TOKEN_LOCAL_STORAGE_KEY
} from "../AccessTokenRepository"
import moment = require("moment")
import * as sinon from 'sinon'
import {AccessTokenRefresher} from "../AccessTokenRefresher";

const clientId = 'client_id'
const clientSecret = 'client_secret'
const scormPorxyUrl = 'https://scorm-proxy.test'
const repository = new AccessTokenRepository(clientId, clientSecret, scormPorxyUrl)

afterEach(() => {
    localStorage.clear()
    sinon.restore()
})

it('token is set and is not expired', () => {
    const refreshTokenStub = sinon.stub(AccessTokenRefresher.prototype, 'refreshToken')

    const token = 'token'
    localStorage.setItem(TOKEN_LOCAL_STORAGE_KEY, token)
    localStorage.setItem(TOKEN_EXPIRES_AT_LOCAL_STORAGE_KEY, moment().add(1, 'year').toISOString())

    const result = repository.getToken()

    expect(result).toEqual(token)
    sinon.assert.notCalled(refreshTokenStub)
})

it('token is not set', () => {
    const token = 'token'
    const refreshTokenStub = sinon.stub(AccessTokenRefresher.prototype, 'refreshToken').returns({
        token,
        expiresAt: moment().add(1, 'year').toISOString(),
    })

    const result = repository.getToken()

    expect(result).toEqual(token)
    sinon.assert.calledOnce(refreshTokenStub)
});

it('token is set but expired', () => {
    const token = 'token'
    const refreshTokenStub = sinon.stub(AccessTokenRefresher.prototype, 'refreshToken').returns({
        token,
        expiresAt: moment().add(1, 'year').toISOString(),
    })

    localStorage.setItem(TOKEN_LOCAL_STORAGE_KEY, 'old token')
    localStorage.setItem(TOKEN_EXPIRES_AT_LOCAL_STORAGE_KEY, moment().subtract(1, 'year').toISOString())

    const result = repository.getToken()

    expect(result).toEqual(token)
    sinon.assert.calledOnce(refreshTokenStub)
});
