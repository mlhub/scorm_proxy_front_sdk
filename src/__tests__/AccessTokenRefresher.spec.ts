import * as sinon from 'sinon'
import {AccessTokenRefresher} from "../AccessTokenRefresher"
import * as moment from "moment";

const clientId = 'client_id'
const clientSecret = 'client_secret'
const scormProxyUrl = 'https://scorm-proxy.test'
const refresher = new AccessTokenRefresher(scormProxyUrl, clientId, clientSecret)

afterEach(() => {
    sinon.restore()
})

it('request successful', () => {
    const token = 'new_token'
    const expiresAt = moment().add(1, 'year').toISOString()
    const mockXHR = {
        open: sinon.stub(),
        send: sinon.stub(),
        setRequestHeader: sinon.stub(),
        status: 200,
        responseText: JSON.stringify({
            access_token: token,
            expires_at: expiresAt,
        })
    }
    const oldXMLHttpRequest = window.XMLHttpRequest;
    // @ts-ignore
    window.XMLHttpRequest = () => mockXHR;

    const result = refresher.refreshToken()

    expect(result.token).toEqual(token)
    expect(result.expiresAt).toEqual(expiresAt)
    sinon.assert.calledOnce(mockXHR.open)
    sinon.assert.calledWith(mockXHR.open, 'POST', `${scormProxyUrl}/api/auth/login`, false)
    sinon.assert.calledOnce(mockXHR.send)
    sinon.assert.calledWith(mockXHR.send, JSON.stringify({
        client_id: clientId,
        client_secret: clientSecret,
    }))
    sinon.assert.calledTwice(mockXHR.setRequestHeader)
    sinon.assert.calledWith(mockXHR.setRequestHeader, 'Content-type', 'application/json')
    sinon.assert.calledWith(mockXHR.setRequestHeader, 'Accept', 'application/json')

    window.XMLHttpRequest = oldXMLHttpRequest
});

it('request failed', () => {
    const mockXHR = {
        open: sinon.stub(),
        send: sinon.stub(),
        setRequestHeader: sinon.stub(),
        status: 422,
        responseText: 'request failed by any reason',
    }
    const oldXMLHttpRequest = window.XMLHttpRequest;
    // @ts-ignore
    window.XMLHttpRequest = () => mockXHR;

    expect(() => {
        refresher.refreshToken()
    }).toThrow()

    window.XMLHttpRequest = oldXMLHttpRequest
});
