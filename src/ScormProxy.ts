import {ScormCompletionPayload} from "./types/ScormCompletionPayload";
import {EVENT_TYPES} from "./eventTypes";
import {AccessTokenRepository} from "./AccessTokenRepository";

export class ScormProxy {
    private scormProxyUrl: string;
    private scormProxyClientId: string;
    private scormProxyClientSecret: string;
    private listeners: {[key: string]: (event: MessageEvent) => void;}  = {};

    private accessTokenRepository: AccessTokenRepository;

    constructor(scormProxyUrl: string, scormProxyClientId: string, scormProxyClientSecret: string) {
        this.scormProxyUrl = scormProxyUrl;
        this.scormProxyClientId = scormProxyClientId;
        this.scormProxyClientSecret = scormProxyClientSecret;
        this.accessTokenRepository = new AccessTokenRepository(scormProxyUrl, scormProxyClientId, scormProxyClientSecret);
    }

    public getUrl(zipArchiveUrl: string, courseData:any = {}): string {
        if(JSON.stringify(courseData) !== '{}')
        {
            return `${this.scormProxyUrl}/open_scorm?url=${zipArchiveUrl}&is_ready=0&course_id=${courseData.id}&user_id=${courseData.user_id}&token=${this.accessTokenRepository.getToken()}`;
        }
        else
        {
            return `${this.scormProxyUrl}/open_scorm?url=${zipArchiveUrl}&token=${this.accessTokenRepository.getToken()}`;
        }

    }

    public getUrlAgain(zipArchiveUrl:string, courseData:any) {
        return `${this.scormProxyUrl}/open_scorm?url=${zipArchiveUrl}&is_ready=1&course_id=${courseData.id}&user_id=${courseData.user_id}&token=${this.accessTokenRepository.getToken()}`;
    };

    public listenCourseCompleted(zipArchiveUrl: string, callback: (payload: ScormCompletionPayload) => void): void {
        const listener = (event: MessageEvent) => {
            if (event.data.zipUrl !== zipArchiveUrl) {
                return;
            }

            switch (event.data.type) {
                case EVENT_TYPES.SCORM_COMPLETED:
                    callback(event.data.data);
                    break;
            }
        };

        if (this.listeners[EVENT_TYPES.SCORM_COMPLETED]) {
            window.removeEventListener('message', this.listeners[EVENT_TYPES.SCORM_COMPLETED]);
        }
        window.addEventListener('message', listener);
        this.listeners[EVENT_TYPES.SCORM_COMPLETED] = listener;
    }

    public flushListeners(): void {
        for (const listener of Object.values(this.listeners)) {
            window.removeEventListener('message', listener);
        }
    }
}
