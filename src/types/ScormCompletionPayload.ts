
export type ScormCompletionPayload = {
    passed: boolean,
    score: number,
    // todo: add better typing
    questionResults: any[],
}
