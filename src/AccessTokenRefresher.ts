
export type RefreshTokenResult = {
    token: string,
    expiresAt: string,
}

export class AccessTokenRefresher {
    private readonly scormProxyUrl: string;
    private readonly scormProxyClientId: string;
    private readonly scormProxyClientSecret: string;

    constructor(scormProxyUrl: string, scormProxyClientId: string, scormProxyClientSecret: string) {
        this.scormProxyUrl = scormProxyUrl;
        this.scormProxyClientId = scormProxyClientId;
        this.scormProxyClientSecret = scormProxyClientSecret;
    }

    public refreshToken(): RefreshTokenResult {
        const request = new XMLHttpRequest();
        request.open('POST', `${this.scormProxyUrl}/api/auth/login`, false)
        request.setRequestHeader('Content-type', 'application/json');
        request.setRequestHeader('Accept', 'application/json');
        request.send(JSON.stringify({
            client_id: this.scormProxyClientId,
            client_secret: this.scormProxyClientSecret,
        }));

        if (request.status !== 200) {
            throw new Error('refresh token failed. ' + request.responseText);
        }

        const response = JSON.parse(request.responseText);

        return {
            token: response.access_token,
            expiresAt: response.expires_at,
        }
    }
}
